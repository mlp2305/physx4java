package net.physx4java.dynamics.collision;
import net.physx4java.Functions;
import net.physx4java.WorldPhysX;
import net.physx4java.dynamics.actors.Actor;

public class CollisionDisabler {
	public static void disableCollision(WorldPhysX world, String actorContainsName) {
		for (Actor actor : world.getAllActors()) {
			if (actor.getName() != null)
				if (actor.getName().contains(actorContainsName)) {
					disableCollision(world, actor);
				}
		}
		// Actor actor2 = WorldPhysX.getActor(actor1Name);
	}
	
	public static void disableCollision(WorldPhysX world, String actorContainsName1, String actorContainsName2) {
		for (Actor actor1 : world.getAllActors()) {
			for (Actor actor2 : world.getAllActors()) {
				if (actor1.getName() != null & actor2.getName() != null)
					if (actor1.getName().contains(actorContainsName1) & actor2.getName().contains(actorContainsName2) | actor1.getName().contains(actorContainsName2) & actor2.getName().contains(actorContainsName1)) {
						world.setContactPairFlags(actor1, actor2, Functions.NxContactPairFlag.NX_IGNORE_PAIR.getValue());
					}
			}
		}
		// Actor actor2 = WorldPhysX.getActor(actor1Name);
	}
	public static void disableCollision(WorldPhysX world, Actor actor) {
		if (actor == null)
			return;
		for (Actor actor2 : world.getAllActors()) {
			world.setContactPairFlags(actor, actor2, Functions.NxContactPairFlag.NX_IGNORE_PAIR.getValue());
		}
		// Actor actor2 = WorldPhysX.getActor(actor1Name);
	}
}

