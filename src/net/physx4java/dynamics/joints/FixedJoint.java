package net.physx4java.dynamics.joints;

import net.physx4java.Functions;
import net.physx4java.WorldPhysX;
import net.physx4java.dynamics.actors.Actor;

public class FixedJoint extends Joint{

	@Override
	public int getNumberOfAxis() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public float getRotation(int axisNumber) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void setDesiredSpeed(int axis, float speed, float force) {
		// TODO Auto-generated method stub
		
	}
	public FixedJoint(WorldPhysX world,String name,FixedJointDesc desc) {
		super(desc);
		this.name = name;
		Functions.jointFixedCreate(world.getId(),getId(), getId(getJointDesc().actor1), getId(getJointDesc().actor2));
		
	}
	public int getId(Actor actor) {
		if(actor==null) return -1;
		return actor.getId();
	}
	
}
