package net.physx4java.dynamics.actors;

import net.physx4java.Functions;
import net.physx4java.WorldPhysX;

public class GroundPlaneActor extends Actor{

	public GroundPlaneActor(WorldPhysX world,String name,float den,float skinWidth) {
		super(world);
		Functions.actorCreateAsGroundPlane(world.getId(),id,den,skinWidth);
		setName(name);
		world.addActor(this);
	}
	 
	
}
