package net.physx4java.dynamics.actors;
import net.physx4java.Functions;
import net.physx4java.WorldPhysX;

public class BoxActor extends Actor {
	public float x, y, z;
	public BoxActor(ActorParameters parameters, WorldPhysX world, float x, float y, float z, float skinWidth) {
		super(world);
		this.x = x;
		this.y = y;
		this.z = z;
		Functions.actorCreateAsBoxShape(world.getId(), id, parameters.isDynamic(), parameters.isUseCCD(), parameters.getDensity(), x, y, z, skinWidth);
	}
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	public float getZ() {
		return z;
	}
}
