package net.physx4java.dynamics.actors;
import net.physx4java.Functions;
import net.physx4java.WorldPhysX;

public class Material {
	float dynamicFricion;
	float staticFricion;
	float restitution;
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "DynamicFriction = " + getDynamicFriction() + " StaticFriction = " + getStaticFriction();
	}
	int id;
	static int id_counter;
	public Material() {
		super();
	}
	public Material(int sid) {
		super();
		id = id_counter++;
		Functions.materialCreateMaterial(sid, id);
		// create
	}
	public Material(WorldPhysX world) {
		this(world.getId());
	}
	public Material(WorldPhysX world, float staticFriction, float dynamicFriction, float restitution) {
		this(world.getId());
		setStaticFriction(staticFriction);
		setDynamicFriction(dynamicFriction);
		setRestitution(restitution);
	}
	public Material(WorldPhysX world, float friction) {
		this(world.getId());
		setStaticFriction(friction);
		setDynamicFriction(friction);
		setRestitution(0);
	}
	
	public float getDynamicFriction() {
		return dynamicFricion;
	}
	public int getId() {
		return id;
	}
	public float getRestitution() {
		return restitution;
	}
	public float getStaticFriction() {
		return staticFricion;
	}
	public void setDynamicFriction(float dynamicFriction) {
		Functions.materialSetDynamicFriction(id, dynamicFriction);
		this.dynamicFricion = dynamicFriction;
	}
	public void setRestitution(float restitution) {
		Functions.materialRestitution(id, restitution);
		this.restitution = restitution;
	}
	public void setStaticFriction(float staticFriction) {
		Functions.materialSetStaticFriction(id, staticFriction);
		this.staticFricion = staticFriction;
	}
}
