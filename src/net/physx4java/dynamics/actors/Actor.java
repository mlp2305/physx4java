package net.physx4java.dynamics.actors;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import net.physx4java.Functions;
import net.physx4java.WorldPhysX;

/**
 * NxActor is the main simulation object in the physics SDK. The actor is owned
 * by and contained in a NxScene. An actor may optionally encapsulate a dynamic
 * rigid body by setting the body member of the actor's descriptor when it is
 * created. Otherwise the actor will be static (fixed in the world). Creation
 * Instances of this class are created by calling NxScene::createActor() and
 * deleted with NxScene::releaseActor(). See NxActorDescBase for a more detailed
 * description of the parameters which can be set when creating an actor.
 * 
 * @author MikL
 * 
 */
public abstract class Actor {
	int id;
	Vector3f forceToAdd;
	static int id_counter = 0;
	String name;
	Material material;
	WorldPhysX world;
	Vector3f lastPosition;
	public WorldPhysX getWorld() {
		return world;
	}
	public void setWorld(WorldPhysX world) {
		this.world = world;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material m) {
		Functions.actorSetMaterial(id, m.getId());
		material = m;
	}
	/**
	 * Applies a force (or impulse) defined in the global coordinate frame to
	 * the actor.
	 * 
	 * @param force_x
	 * @param force_y
	 * @param force_z
	 */
	public void addForce(float x, float y, float z) {
		Functions.actorAddForce(id, x, y, z);
	}
    public void setCCDMotionThreshold(float mt) {
        Functions.actorSetCCDMotionThreshold(id,mt);
    }
	/**
	 * Gets the id of the actor
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	public Matrix3f getRotation() {
		return new Matrix3f(Functions.actorGetRotation(id));
	}
	public float[] getRotationAsArray() {
		return Functions.actorGetRotation(id);
	}
	public void setSolverIterationCounts(int iterations) {
		Functions.actorSetSolverIterations(id, iterations);
	}
	Matrix3f tempRotation;
	public void setRotation(Matrix3f m) {
		tempRotation = m;
	}
	public void setRotation(float f[]) {
		Functions.actorSetRotation(id, f);
	}
	public void raiseActorFlag(int flag) {
		Functions.actorRaiseFlag(id, flag);
	}
	public void clearActorFlag(int flag) {
		Functions.actorClearFlag(id, flag);
	}
	public boolean readActorFlag(int flag) {
		return Functions.actorReadFlag(id, flag);
	}
	public void enableKinematic() {
		Functions.actorEnableKinematic(id);
	}
	public void disableKinematic() {
		// Functions.actorDisableKinematic(id);
	}
	/**
	 * Creates a new actor.
	 * 
	 */
	public Actor(WorldPhysX world) {
		// create id
		id = id_counter++;
		setWorld(world);
		world.addActor(this);
	}
	public void moveToGlobalPose(Vector3f pos, Matrix3f m) {
		Functions.actorMoveToGlobalPose(id, pos, m);
	}
	/**
	 * Sets the mass of a dynamic actor.
	 * 
	 * @param mass
	 *            the mass
	 */
	public void setMass(float mass) {
		Functions.actorSetMass(id, mass);
	}
	/**
	 * Retrieves the mass of the actor.
	 * 
	 * @return
	 */
	public double getMass() {
		return Functions.actorGetMass(id);
	}
	Vector3f tempPosition;
	public void setPosition(float x, float y, float z) {
		setPosition(new Vector3f(x, y, z));
	}
	public void update() {
		lastPosition = getPosition();
		if (lastPosition != null) {
		}
		if (tempPosition != null) {
			Functions.actorSetPosition(id, tempPosition.x, tempPosition.y, tempPosition.z);
			tempPosition = null;
		}
		if (tempRotation != null) {
			Functions.actorSetRotation(id, tempRotation);
			tempRotation = null;
		}
		if(forceToAdd!=null) {
			Functions.actorAddForce(id, forceToAdd.x, forceToAdd.y, forceToAdd.z);
			//System.out.println("ADDING FORCE "+forceToAdd);
			forceToAdd = null;
		}
		
	}
	
	public void setPosition(Vector3f vec) {
		tempPosition = vec;
	}
	public void recomputeAdaptiveForceCounters() {
		Functions.actorRecomputeAdaptiveForceCounters(id);
	}
	public void setPosition(float f[]) {
		setPosition(f[0], f[1], f[2]);
	}
	public void setLinearVelocity(float x, float y, float z) {
		Functions.actorSetLinearVelocity(id, x, y, z);
	}
	public void setLinearVelocity(float f[]) {
		Functions.actorSetLinearVelocity(id, f[0], f[1], f[2]);
	}
	public Vector3f getLinearVelocity() {
		return new Vector3f(Functions.actorGetLinearVelocity(id));
	}
	public float[] getLinearVelocityAsArray() {
		return Functions.actorGetLinearVelocity(id);
	}
	public void setAngularVelocity(float x, float y, float z) {
		Functions.actorSetAngularVelocity(id, x, y, z);
	}
	public void setAngularVelocity(float[] f) {
		Functions.actorSetAngularVelocity(id, f[0], f[1], f[2]);
	}
	public Vector3f getAngularVelocity() {
		return new Vector3f(Functions.actorGetAngularVelocity(id));
	}
	public float[] getAngularVelocityAsArray() {
		return Functions.actorGetAngularVelocity(id);
	}
	public void setLinearDamping(float damping) {
		Functions.actorSetLinearDamping(id, damping);
	}
	public float getLinearDamping() {
		return Functions.actorGetLinearDamping(id);
	}
	public void setAngularDamping(float damping) {
		Functions.actorSetAngularDamping(id, damping);
	}
	public float getAngularDamping() {
		return Functions.actorGetAngularDamping(id);
	}
	public void setLinearMomentum(float x, float y, float z) {
		Functions.actorSetLinearMomentum(id, x, y, z);
	}
	public void setLinearMomentum(float f[]) {
		Functions.actorSetLinearMomentum(id, f[0], f[1], f[2]);
	}
	public Vector3f getLinearMomentum() {
		return new Vector3f(Functions.actorGetLinearMomentum(id));
	}
	public float[] getLinearMomentumAsArray() {
		return Functions.actorGetLinearMomentum(id);
	}
	public void setAngularMomentum(float x, float y, float z) {
		Functions.actorSetAngularMomentum(id, x, y, z);
	}
	public void setAngularMomentum(float[] f) {
		Functions.actorSetAngularMomentum(id, f[0], f[1], f[2]);
	}
	public Vector3f getAngularMomentum() {
		return new Vector3f(Functions.actorGetAngularMomentum(id));
	}
	public float[] getAngularMomentumAsArray() {
		return Functions.actorGetAngularMomentum(id);
	}
	public double computeKineticEnergy() {
		return Functions.actorComputeKineticEnergy(id);
	}
	public Vector3f getPosition() {
		return new Vector3f(Functions.actorGetPosition(id));
	}
	public float[] getPositionAsArray() {
		return Functions.actorGetPosition(id);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Vector3f getAcceleration() {
		Vector3f acc = new Vector3f();
		Vector3f pos = getPosition();
		acc.x = Math.abs(lastPosition.x - pos.x);
		acc.y = Math.abs(lastPosition.y - pos.y);
		acc.z = Math.abs(lastPosition.z - pos.z);
		return acc;
	}
}
