package net.physx4java.dynamics.actors;

import net.physx4java.Functions;
import net.physx4java.WorldPhysX;

public class CapsuleActor extends Actor{
	float radius;
	float height;
	public CapsuleActor( ActorParameters parameters,WorldPhysX world,float height,float radius,float skinWidth) {
		super(world);
		this.radius = radius;
		this.height = height;
		//parameters.setUseCCD(false);
		Functions.actorCreateAsCapsuleShape(world.getId(),id,parameters.isDynamic(),parameters.isUseCCD(),parameters.getDensity(),radius,height,skinWidth);
	}
	public float getHeight() {
		return height;
	}
	
	public float getRadius() {
		return radius;
	}
	
}
