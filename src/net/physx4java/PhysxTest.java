package net.physx4java;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import net.physx4java.dynamics.actors.ActorParameters;
import net.physx4java.dynamics.actors.BoxActor;

public class PhysxTest extends JFrame {
	JTextArea textArea = new JTextArea();
	public PhysxTest() throws HeadlessException {
		super();
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				super.windowClosing(e);
				System.exit(0);
			}
		});
		textArea.setEditable(false);
		getContentPane().add(textArea);
		setBounds(400, 200, 800, 600);
		setVisible(true);
		addText("Loading PhysX..");
		pause(3);
		WorldPhysX world = null;
		try {
			world = new WorldPhysX();
		} catch (UnsatisfiedLinkError e) {
			addText("PhysX could not be loaded :");
			addText(e.toString());
			return;
		}
		addText("PhysX loaded..");
		addText("Testing PhysX");
		pause(3);
		BoxActor a = new BoxActor(new ActorParameters(1000, false, true), world, 0, 100, 0, 0);
		a.setMass(200);
		world.addActor(a);
		world.setGravity(0, -100, 0);
		for (int i = 0; i < 20; i++) {
			world.step(0.01f);
			addText("Testing falling actor, position = " + a.getPosition());
			a.update();
		}
		addText("Test completed, no failure");
	}
	public void pause(int seconds) {
		try {
			Thread.sleep(1000 * seconds);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void addText(String text) {
		textArea.append(text + "\n");
	}
	public static void main(String[] args) {
		new PhysxTest();
	}
}
