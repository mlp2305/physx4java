package net.physx4java;


import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import net.physx4java.dynamics.actors.Actor;
import net.physx4java.dynamics.collision.CollisionListener;

import com.google.inject.Inject;

public class WorldPhysX {
	static int sceneId;
	int id;
	long stepNumber;
	float stepSize;
	HashMap<Integer, Actor> actorsById = new HashMap<Integer, Actor>();
	HashMap<String, Actor> actorsByName = new HashMap<String, Actor>();
	static HashMap<Integer, Actor> allActors = new HashMap<Integer, Actor>();
	@Inject(optional = true)
	CollisionListener listener;
	boolean deleted = false;

	public Actor[] getAllActorsAsArray() {
		Actor[] a = new Actor[getAllActors().size()];
		Iterator<Actor> i = getAllActors().iterator();
		int count = 0;
		while (i.hasNext()) {
			a[count++] = i.next();
		}
		return a;
	}

	
	
	
	public void setListener(CollisionListener c) {
		this.listener = c;
	}

	public static Actor getActorById(int id) {
		return allActors.get(id);
	}

	public Collection<Actor> getAllActors() {
		return actorsById.values();
	}

	public CollisionListener getListener() {
		return listener;
	}

	public void addActor(Actor actor) {
		actorsById.put(actor.getId(), actor);
		actorsByName.put(actor.getName(), actor);
		allActors.put(actor.getId(), actor);
	}

	public void setGravityY(float gravityY) {
		Functions.worldSetGravity(id, 0, gravityY, 0);
	}

	public Actor getActor(int id) {
		return actorsById.get(id);
	}

	public Actor getActor(String name) {
		return actorsByName.get(name);
	}

	public WorldPhysX() {
		super();
		loadLibraries();
		id = sceneId++;
		Functions.worldCreate(id);
		enableContiniousCollisonDetectionD();
	}

	public void loadLibraries() {
		// System.loadLibrary("glut32sdasd");
		String path = System.getProperty("org.lwjgl.librarypath");
		String root = null;
		if (path != null) {
			System.setProperty("java.library.path", path);
			root = path + "\\";
		}

		boolean bit_64 = System.getProperty("sun.arch.data.model").equals("64");
		bit_64 = false;
		if (bit_64) {
			System.loadLibrary("cudart64_30_9");
			System.loadLibrary("PhysXLoader64");
			System.loadLibrary("PhysXCore64");
			System.loadLibrary("PhysXCooking64");
			System.loadLibrary("PhysXDevice64");
			System.loadLibrary("physx4j64");
		} else {
			System.loadLibrary("cudart32_30_9");
			System.loadLibrary("PhysXLoader");
			System.loadLibrary("PhysXCore");
			System.loadLibrary("PhysXCooking");
			System.loadLibrary("PhysXDevice");
			System.loadLibrary("physx4j");
		}
		/*
		 * if (root != null) { System.load(root + "physx4j.dll");
		 * System.out.println("LOAD() PHYSX SKOD"); System.load(root +
		 * "cudart32_30_9.dll"); System.load(root + "PhysXLoader.dll");
		 * System.load(root + "PhysXCore.dll"); System.load(root +
		 * "PhysXCooking.dll"); System.load(root + "PhysXDevice.dll");
		 * 
		 * } else { System.out.println("LOADLIBRARY() PHYSX");
		 * 
		 * System.loadLibrary("cudart32_30_9");
		 * System.loadLibrary("PhysXLoader"); System.loadLibrary("PhysXCore");
		 * System.loadLibrary("PhysXCooking");
		 * System.loadLibrary("PhysXDevice");
		 * System.loadLibrary("physx4java64");
		 */
	}

	public void setGravity(float x, float y, float z) {
		Functions.worldSetGravity(id, x, y, z);
	}

	public void enableContiniousCollisonDetectionD() {
		// Functions.worldUseCCD();
	}

	public void setTiming(float timing, int substeps) {

		Functions.worldSetStepTiming(id, timing, substeps);

	}

	public void step() {
		step(stepSize);
	}

	boolean paused = false;

	public boolean isPaused() {
		return paused;
	}

	public void setPaused(boolean paused) {
		this.paused = paused;
	}

	public void step(float step) {
		if (deleted)
			return;
		if (paused)
			return;
		// System.out.println("STEPPING");
		Functions.worldStep(id, step);
		// System.out.println("STEPPED");
		stepNumber++;
	}

	public void flushCaches() {
		Functions.worldFlushCaches(id);
	}

	public float getGravityX() {
		return Functions.worldGetGravityX(id);
	}

	public void setEnableContactUserReport(boolean enable) {
		Functions.worldEnableUserContactReport(id);
		;
	}

	public void enableContactUserReport() {
		Functions.worldEnableUserContactReport(id);
		;
	}

	public void setContactPairFlags(Actor a1, Actor a2, int flags) {
		Functions.worldSetContactPairFlags(id, a1.getId(), a2.getId(), flags);
	}

	public float getGravityY() {
		return Functions.worldGetGravityY(id);
	}

	public float getGravityZ() {
		return Functions.worldGetGravityZ(id);
	}

	public int getId() {
		return id;
	}

	public void raiseFlag(int flag) {
		Functions.worldRaiseFlag(id, flag);
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getStepSize() {
		return stepSize;
	}

	public void setStepSize(float stepSize) {
		this.stepSize = stepSize;
	}

	public long getStepNumber() {
		return stepNumber;
	}

	public void setStepNumber(long stepNumber) {
		this.stepNumber = stepNumber;
	}

	public void setSkinWidth(float skinWidth) {
		Functions.worldSetSkinWidth(skinWidth);
	}

	public void delete() {
		if (!deleted)
			Functions.worldDelete(id);
		actorsById.clear();
		actorsByName.clear();
		allActors.clear();
	}

	public static void main(String[] args) {
		new WorldPhysX();
		// System.loadLibrary("physx4j64");
	}
}
